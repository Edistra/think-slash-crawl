'use strict';

var FloorObject  = require('client/gameObjects/FloorObject');
var MapTiles 	 = require('client/gameObjects/MapTiles');
var EntityObject = require('client/gameObjects/EntityObject');
var TurnManager  = require('client/gameObjects/TurnManager');
var UI 			 = require('client/gameSprites/UI');

function Play(){};

Play.prototype = {
	keys : null,
	preload : function(){
    	this.game.load.script('Gray', 'gameAssets/filters/Gray.js');
    	this.game.load.script('Marble', 'gameAssets/filters/Marble.js');
        this.game.load.image('icon_shape', 'gameAssets/icon_shape.png');
        this.game.load.image('icon_5', 'gameAssets/icon_player.png');
        this.game.load.image('icon_40', 'gameAssets/icon_enemy.png');
        this.game.load.image('icon_41', 'gameAssets/icon_enemy.png');
        this.game.load.image('door', 'gameAssets/door_wood.png');
        this.game.load.image('shadow', 'gameAssets/Shadow_noBG.png');
        this.game.load.image('cube', 'gameAssets/cube.png');
   		this.game.load.image('tile', 'gameAssets/tile.png');
   		this.game.load.image('chest', 'gameAssets/chestnormal.png');
   		this.game.load.image('chest_christmas', 'gameAssets/chestchristmas.png');
		this.load.atlasJSONArray('knight', 'gameAssets/knight.png', 'gameAssets/knight.json');
		this.load.atlasJSONArray('moonlight_flower', 'gameAssets/moonlight_flower.png', 'gameAssets/moonlight_flower.json');
		//this.load.atlasJSONArray('moonlight_flower', 'gameAssets/moon_moon.png', 'gameAssets/moon_moon.json');
		this.load.atlasJSONArray('orc', 'gameAssets/orc.png', 'gameAssets/orc.json');
		this.load.atlasJSONArray('jakk_xmas', 'gameAssets/monster/jakk_xmas.png', 'gameAssets/monster/jakk_xmas.json');
		this.load.atlasJSONArray('cursor', 'gameAssets/cursor/cursor.png', 'gameAssets/cursor/cursor.json');

		this.load.atlasJSONArray('tileset', 'gameAssets/tileset/tileset.png', 'gameAssets/tileset/tileset.json');
		this.load.atlasJSONArray('minecraft_tiles', 'gameAssets/minecraft_tiles.png', 'gameAssets/minecraft_tiles.json');

		this.game.world.setBounds(0, 0, 2048, 1024);

		this.game.time.advancedTiming = true;
        this.game.plugins.add(new Phaser.Plugin.Isometric(this.game));
       	this.game.iso.anchor.setTo(0.5, 0.2);
       	this.game.stage.smoothed = false;

       	this.game.load.json('floor', 'gameAssets/maps/floor.json');
       	this.game.load.json('grid', 'gameAssets/maps/grid.json');
	},

	create : function(){
		console.log(this);
		var me = this;
		this.game.stage.backgroundColor = '#333';
		this.game.tileSize = 36;
		this.game.tilesLayer = this.game.add.group();
		this.game.entitiesLayer = this.game.add.group();
		
    	this.game.ui = new UI(this.game);
    	this.game.turnManager = new TurnManager(this.game);
		this.game.floor = new FloorObject(this.game);
		this.game.floor.initMapTiles();

	    this.game.input.onDown.add(this.game.mapTiles.onGridClick, this.game.mapTiles);
        //if(this.game.player != undefined)	this.game.camera.follow(this.game.player.sprite);

        this.game.turnManager.startTurn();
        /* Cursor */
    	this.game.cursorPos = new Phaser.Plugin.Isometric.Point3();

    	this.game.cursor = this.game.add.sprite(0,0,'cursor');
    	this.game.cursor.anchor.setTo(0.2,0.15);
    	this.game.cursor.scrollFactor = 0;
    	this.game.cursor.animations.add('pointer',["pointer_01","pointer_02","pointer_03","pointer_04","pointer_05","pointer_06"],5,true);
    	this.game.cursor.animations.add('attack',["attack"],5,true);
    	this.game.cursor.animations.add('door',["door_01","door_02","door_03","door_04","door_05"],5,true);
    	this.game.cursor.animations.play('pointer');

        this.game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR).onDown.add(function(){
        	console.log('end turn');
        	me.game.turnManager.endTurn(); 
        });

    	this.keys = this.game.input.keyboard.createCursorKeys();
	},

	update : function(){
		if (this.keys.left.isDown) {
		    this.game.camera.x -= 4;
		}
		if (this.keys.right.isDown) {
		    this.game.camera.x += 4;
		}
		if (this.keys.up.isDown) {
		    this.game.camera.y -= 4;
		}
		if (this.keys.down.isDown) {
			this.game.camera.y += 4;
		}
       	document.querySelector('#game').style.cursor = 'none';
		this.game.cursor.visible = this.game.input.mousePointer.withinGame;
		//if(!this.game.player.moving){
			this.game.cursor.x = this.game.input.activePointer.worldX;
			this.game.cursor.y = this.game.input.activePointer.worldY;
		//}
        this.game.iso.unproject(this.game.input.activePointer.position, this.game.cursorPos);

        this.game.mapTiles.update();
	},

    render: function () {
    	if(this.game.turnManager)	this.game.debug.text(this.game.turnManager.currentEntity.sprite.key + " turn", 2, 14, "#fff");
    }
}

module.exports = Play;