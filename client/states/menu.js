'use strict';

function Menu(){};

Menu.prototype = {
	titleText : null,
	playButton : null,
	filter : null,
	background : null,
	icon : null,
	block : null,
	colors : ['#FDCBDE','#F4F4A6','#77dd77','#333333','#FFFFFF', '#afd5ec', '#eeb489'],
	preload : function(){
    	this.game.load.script('Gray', 'gameAssets/Gray.js');
        this.game.load.image('icon_5', 'gameAssets/icon_player.png');
        this.game.load.image('mask', 'gameAssets/mask.png');
    	this.game.load.script('filter', 'https://cdn.rawgit.com/photonstorm/phaser/master/filters/Fire.js');
		this.load.atlasJSONArray('minecraft_tiles', 'gameAssets/minecraft_tiles.png', 'gameAssets/minecraft_tiles.json');
	},

	create : function(){
    	this.stage.backgroundColor = this.colors[3];
    	var minimumScreenSize = Math.min(this.game.width,this.game.height);
		//this.add.button(0,0,'menu',this.startGame,this);
		var textStyle = {font:"bold "+(minimumScreenSize/10)+"px sans-serif",fill:this.colors[4],align:"center",boundsAlignH: "center", boundsAlignV: "middle"};
		this.titleText = this.game.add.text(this.game.width/2,60,'Phaser / Socket.io',textStyle);
		this.titleText.anchor.set(0.5);
		var textStyle = {font:"bold "+(minimumScreenSize/15)+"px sans-serif",fill:this.colors[4],align:"center",boundsAlignH: "center", boundsAlignV: "middle"};

		var bmd = this.add.bitmapData(minimumScreenSize/4,minimumScreenSize/4);
        bmd.ctx.fillStyle = this.colors[4];
        bmd.ctx.beginPath();
		bmd.ctx.arc(bmd.width/2, bmd.height/2, bmd.width/2, 0, Math.PI*2, true); 
		bmd.ctx.closePath();
		bmd.ctx.fill();
		this.playButton = this.game.add.button(this.game.width/2, this.game.height/2, bmd, this.startGame, this, 1, 0, 2);
		this.playButton.anchor.set(0.5);
		this.game.input.onDown.add(this.startGame, this);


        this.icon = this.game.add.sprite(250, 250,'icon_5');
        //icon.fixedToCamera = true;

        var grayfilter = this.game.add.filter('Gray');

        this.icon.filters = [grayfilter];
		/*this.background = this.game.add.sprite(0, 0);
		this.background.width = 800;
		this.background.height = 600;
        this.filter = this.game.add.filter('Fire',800,600);
        this.background.filters = [this.filter];*/

        this.block = this.game.add.sprite(500, 500,'minecraft_tiles', 'goldblock');
        this.block.anchor.setTo(0.5);
       	//var mask = this.game.add.sprite(0, 0,'mask');
        //mask.anchor.setTo(0.5);

       /* var maskGraphics = this.game.add.graphics(0,0);
        maskGraphics.beginFill(0xffffff);
        maskGraphics.drawEllipse(block.x,block.y - h/2,w/3,h/1.5);
        console.log(maskGraphics);*/
        //var bmd = this.game.make.bitmapData(49,88);
        //bmd.alphaMask('icon_5','mask');
        //var thing = new PIXI.Graphics();
        var block = this.block;

        var w = block.width;
        var h = block.height;
        var thing = this.game.add.graphics(block.x - w/2,block.y-h/2);
        thing.beginFill(0xFFFFFF);
        thing.moveTo(0,0);
        thing.lineTo(w,0);
        thing.lineTo(w,h);
        thing.lineTo(w/2 + w/10,h);
        thing.bezierCurveTo(w/1.2,h/1.5,w/1.2,h/3,w/2 + w/10,0);
        thing.lineTo(w/2 - w/10,0);
        thing.bezierCurveTo(w/5,h/3,w/5,h/1.5,w/2 - w/10,h);
        thing.lineTo(0,h);
        thing.lineTo(0,0);

        /*thing.moveTo(-w,-h);
        thing.lineTo(w*2,-h);
        thing.lineTo(w*2,h*2);
        thing.lineTo(w/2 + w/10,h*2);
        thing.lineTo(w/2 + w/10,h);
        thing.bezierCurveTo(w/1.2,h/1.5,w/1.2,h/3,w/2 + w/10,0);
        thing.lineTo(w/2 - w/10,0);
        thing.bezierCurveTo(w/5,h/3,w/5,h/1.5,w/2 - w/10,h);

        thing.lineTo(w/2 + w/10,h);
        thing.lineTo(w/2 + w/10,h*2);
        thing.lineTo(-w,h*2);
        thing.lineTo(-w,-h);*/


       // thing.endFill();
        //this.block.mask = thing;
        console.log(thing);
       // block.mask.x -= 10;
        //this.game.add.image(this.game.world.centerX - 400, 320, thing).anchor.set(0.5, 0);
	},

	update : function(){
		//this.filter.update();

    	//this.block.mask.x = this.game.input.activePointer.worldX;
		//this.block.mask.y = this.game.input.activePointer.worldY;
	},

	startGame : function(){
		this.game.state.start('Game');
	}
};

module.exports = Menu;