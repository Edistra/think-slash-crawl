/**
* @author Mat Groves http://matgroves.com/ @Doormat23
*/

/**
* This turns your displayObjects to Glowscale.
* @class Glow
* @contructor
*/
Phaser.Filter.Glow = function (game) {

    Phaser.Filter.call(this, game);

    this.uniforms.Glow = { type: '1f', value: 1.0 };

    this.fragmentSrc = [

        "precision mediump float;",

        "varying vec2 vTextureCoord;",
        "uniform sampler2D uSampler;",

        "void main(void) {",

            "vec4 texColor = texture2D(uSampler, vTextureCoord);",

            "if (vTextureCoord.x < 0.1) {",
                "texColor = vec4(1.0, 0.0, 1.0, 1.0);",
            "}",
     
            "gl_FragColor = texColor;",

        "}"
    ];

};

Phaser.Filter.Glow.prototype = Object.create(Phaser.Filter.prototype);
Phaser.Filter.Glow.prototype.constructor = Phaser.Filter.Glow;

/**
* The strength of the Glow. 1 will make the object black and white, 0 will make the object its normal color
* @property Glow
*/
Object.defineProperty(Phaser.Filter.Glow.prototype, 'Glow', {

    get: function() {
        return this.uniforms.Glow.value;
    },

    set: function(value) {
        this.uniforms.Glow.value = value;
    }

});