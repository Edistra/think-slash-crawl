<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>3</int>
        <key>texturePackerVersion</key>
        <string>4.0.1</string>
        <key>fileName</key>
        <string>/Applications/MAMP/htdocs/phaser_isometric/client/assets/gameAssets/minecraft_tiles.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <true/>
        <key>premultiplyAlpha</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>json-array</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>0</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>reduceBorderArtifacts</key>
        <false/>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">Basic</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">AnySize</enum>
            <key>forceSquared</key>
            <false/>
            <key>forceWordAligned</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>data</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>monk.json</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <false/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>cleanTransparentPixels</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>0</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">None</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>pivotPoint</key>
            <enum type="SpriteSettings::PivotPoint">Center</enum>
        </struct>
        <key>fileList</key>
        <array>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/attack_spell_02.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/dying_01.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/attack_spell_up_06.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/attack_fist_l_06.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/attack_spell_up_04.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/attack_spell_06.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/attack_fist_l_02.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/attack_spell_up_01.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/attack_fist_r_01.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/attack_spell_up_02.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/dying_02.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/attack_spell_04.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/attack_fist_l_07.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/attack_spell_01.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/attack_fist_l_08.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/attack_spell_up_05.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/attack_fist_r_04.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/stand_04.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/attack_fist_l_01.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/attack_fist_r_03.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/stand_02.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/stand_06.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/stand.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/stand_05.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/attack_fist_l_05.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/stand_03.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/dying_up_01.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/attack_fist_r_05.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/attack_fist_r_02.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/dying_03.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/attack_spell_03.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/stand_01.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/dying_up_03.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/attack_fist_l_04.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/attack_fist_r_06.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/attack_spell_05.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/attack_mace_01.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/walk_01.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/dying_up_02.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/walk_07.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/sit.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/attack_fist_l_03.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/walk_05.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/attack_fist_r_up_05.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/attack_mace_02.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/attack_fist_r_up_04.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/stand_up.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/sit_up.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/attack_fist_r_up_06.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/walk_06.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/attack_mace_03.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/walk_02.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/walk_08.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/walk_04.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/attack_fist_l_up_07.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/attack_mace_04.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/attack_fist_l_up_06.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/attack_fist_l_up_08.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/attack_spell_up_03.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/walk_03.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/walk_up_02.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/walk_up_04.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/walk_up_01.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/walk_up_03.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/attack_fist_l_up_02.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/walk_up_05.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/stand_up_05.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/walk_up_06.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/attack_fist_r_up_01.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/walk_up_08.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/attack_fist_l_up_01.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/attack_fist_l_up_05.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/stand_up_04.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/attack_mace_up_01.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/pick_up_01.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/stand_up_01.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/attack_mace_up_03.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/stand_up_03.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/stand_up_02.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/walk_up_07.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/stand_up_06.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/attack_fist_l_up_04.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/attack_mace_up_02.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/pick_01.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/attack_fist_l_up_03.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/attack_mace_up_04.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/attack_fist_r_up_02.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/pick_02.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/attack_fist_r_up_03.png</filename>
            <filename>../../../../../../../Users/Matthieu/Downloads/Sprites/Ragnarok/monk/pick_up_02.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
    </struct>
</data>
