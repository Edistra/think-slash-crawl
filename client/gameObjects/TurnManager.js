'use strict';

var MapTiles     = require('client/gameObjects/MapTiles');

var TurnManager = function(game) {
    this.game = game;
    this.currentEntity = null;
    this.turn = 0;
    this.entities = [];
};

TurnManager.prototype.constructor = TurnManager;

TurnManager.prototype.startTurn = function() {
	var me = this;
    this.setNextEntity();

    console.log('next Entity is '+this.currentEntity.ref);
    if(this.currentEntity.ref != 5 && this.game.player.hp > 0){
        var dst_index_row = Math.floor(this.game.player.sprite.isoY/this.game.tileSize);
        var dst_index_col = Math.floor(this.game.player.sprite.isoX/this.game.tileSize);

    	if(this.currentEntity.getRange(this.game.player.sprite) == 1){
    		console.log('attack !');
    		this.currentEntity.attack(this.game.player);
    		this.game.time.events.add(Phaser.Timer.SECOND * 1, function(){this.endTurn();}, this);
    	}else{
	       this.game.mapTiles.tilesGrid[dst_index_row][dst_index_col] =  0; 
	        this.currentEntity.moveInPath(dst_index_col, dst_index_row,1,function(){
	       		me.game.mapTiles.tilesGrid[dst_index_row][dst_index_col] =  5; 
	       		me.endTurn();
	        });
	    }         
    }
};

TurnManager.prototype.setNextEntity = function() {
    var me = this;
    this.entities.some(function(entity, index) {
        if(!entity.hasPlayed && entity.hp > 0){
        	me.currentEntity = entity;
            return true;
        }
        if(index == (me.entities.length - 1) && me.getNumberAlives() > 1)   me.resetTurns();
    });
};

TurnManager.prototype.getNumberAlives = function() {
    var counter = 0;
    this.entities.some(function(entity, index) {
        if(entity.hp > 0)
        	counter++;
    });
    return counter;
};

TurnManager.prototype.endTurn = function() {
    this.currentEntity.hasPlayed = true;
    this.startTurn();

    this.game.ui.showEntitiesTurns();
};

TurnManager.prototype.resetTurns = function() {
	console.log(this.entities);
    this.entities.some(function(entity) {
        entity.hasPlayed = false;
        entity.mp = entity.max_mp;
        entity.ap = entity.max_ap;
    });
    this.setNextEntity();
};

TurnManager.prototype.resetEntities = function() {
    this.entities = [];
};

TurnManager.prototype.sortEntities = function() {
	this.entities.sort(function(a,b) {
        if(a.ref == 5)	return -1;
        return 1;
    });
};

module.exports = TurnManager;