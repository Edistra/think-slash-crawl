'use strict';

var EntitySprite = require('client/gameSprites/EntitySprite');

var EntityObject = function(game, x, y, z, sprite) {
    this.game = game;
    this.speed = 500;
    this.setupSprite(x, y, z, sprite);
    this.hp = 3;
    this.max_hp = 3;
    this.mp = 3; // movement points
    this.max_mp = 3;
    this.ap = 3; // action points
    this.max_ap = 3;
    this.moving = false;
    this.tile = null; // current tile
    this.ref = 0; // json map index

    if(this.game.turnManager)   this.game.turnManager.entities.push(this); // add entity to turnManager
};

EntityObject.prototype.setupSprite = function(x, y, z, sprite){
    this.sprite = new EntitySprite(this.game, x, y, z, sprite);
    this.setPosition(x,y);
    this.game.add.existing(this.sprite);
    this.sprite.stand();
};

EntityObject.prototype.moveInPath = function(target_col, target_row, range, callback){
	var me = this;
	var pathfinder = this.game.pathfinder;
	var source_row = Math.floor(this.sprite.isoY/this.game.tileSize);
	var source_col = Math.floor(this.sprite.isoX/this.game.tileSize);
    if(!this.game.player.moving){
    	pathfinder.findPath(source_col, source_row, target_col, target_row, function( path ) {
            if(path === null) {
    	        console.log("The path to the destination point was not found.");
    	    } else {
    	    	for(var i = 0; i < path.length; i++) {
    	    		//console.log("P: " + i + ", X: " + path[i].x + ", Y: " + path[i].y);
    	    		if(i > 0) {
    	    			path[i].straight_x = (path[i].x == path[i-1].x);
    	    			path[i].straight_y = (path[i].y == path[i-1].y);
    	    			path[i-1].willTurn = ((path[i].straight_x && path[i-1].straight_y) || (path[i].straight_y && path[i-1].straight_x));	
    	    		}
    	    	}
                if(range > 0){ // if the entity has to stop x tiles before destination
                    while(range > 0){
                        path.pop();
                        range--;
                    }
                }
                var cost = path.length - 1;
                if(cost > (me.mp)){
                    console.log('not enough mp');
                    //return;
                }
    	    	// removing points on straight paths to avoid animation breaks
    	    	for (var i = path.length - 2; i > 0; i--){
        			if(!path[i].willTurn)	path.splice(i,1);
    	    	}
    		   	path.shift(); // remove the first point (current position)
                me.moveTo(0,0,path,callback);
                //me.mp -= cost;
                me.text_mp.text = me.mp+'MP';
    	    }
    	});
    	pathfinder.calculate();
    }
};

EntityObject.prototype.moveTo = function(isoX,isoY,path,callback){
	var me = this;
	var sprite = this.sprite;
    var tile_y, tile_x;

	if(path != undefined && path.length > 0){
        tile_y = path[0].y;
        tile_x = path[0].x;
		path.shift();
	}else{
        tile_y = Math.floor(isoY/this.game.tileSize);
        tile_x = Math.floor(isoX/this.game.tileSize);
    }
	//if(isoX == 0 && isoY == 0) return;
    var tile = this.game.tilesCache[tile_y][tile_x];
	var newIsoX = tile.isoX;
	var newIsoY = tile.isoY;

	me.moving = true;
	me.faceTile(newIsoX,newIsoY);
	sprite.walk();
	var speed = me.speed * (Math.abs(sprite.isoX - newIsoX + sprite.isoY - newIsoY) / 100);
	var t = me.game.add.tween(sprite).to({isoX:newIsoX,isoY:newIsoY},speed, Phaser.Easing.Linear.InOut, true);
    t.onComplete.add(function(){
        me.setTile(tile);
        me.moving = false;
        if(path != undefined && path.length > 0){
       		me.moveTo(0,0,path,callback); // recursive
   		}
		else{
    		if(speed > 1) sprite.stand();
            if(callback != undefined){
                callback();
            }
			console.log('endMovement');
            me.game.ui.cleanPath();
		}
    },me);
};

EntityObject.prototype.setPosition = function(isoX, isoY){
    var speed = this.speed;
    this.speed = 1;
    this.moveTo(isoX,isoY); // moveTo for tile settings
    this.speed = speed;
};

EntityObject.prototype.setTile = function(tile){
    var tileSize = this.game.tileSize;
    if(this.tile != undefined)
        this.game.mapTiles.tilesGrid[this.tile.isoY / tileSize][this.tile.isoX / tileSize] =  0; // free previous tile
    this.game.mapTiles.tilesGrid[Math.floor(tile.isoY / tileSize)][Math.floor(tile.isoX / tileSize)] =  this.ref; // set next tile as occupied
    this.tile = tile; // set next tile
};

EntityObject.prototype.faceTile = function(isoX, isoY){ // turn the entity in front of a specific tile
    this.sprite.ext = '';

    if(this.sprite.isoX < isoX){
        this.sprite.faceRight();
    }
    else if(this.sprite.isoX > isoX){
        this.sprite.ext = '_up';
        this.sprite.faceLeft();
    }
    if(this.sprite.isoY < isoY){
        this.sprite.faceLeft();
    }
    else if(this.sprite.isoY > isoY){
        this.sprite.ext = '_up';
        this.sprite.faceRight();
    }
};

EntityObject.prototype.setAsEnnemy = function(){
	this.sprite.inputEnabled = true;
};

EntityObject.prototype.isHurted = function(){
    var me = this;
    this.hp -= 1;
    if(this.hp > 0)
        this.sprite.hurt();
    else
        this.sprite.dying(function(){
            me.destroy();
        });
};

EntityObject.prototype.isAttacked = function(){ // currently only works for player attacking ennemy
    var me = this;
    this.game.player.attack(this);
   /* this.game.player.faceTile(this.sprite.isoX,this.sprite.isoY);
    this.game.player.sprite.attack(function(){
        me.faceTile(this.game.player.sprite.isoX,this.game.player.sprite.isoY);
        me.isHurted();
    });*/
};

EntityObject.prototype.attack = function(entity){ // currently only works for player attacking ennemy
    var me = this;
    this.faceTile(entity.sprite.isoX,entity.sprite.isoY);
    this.sprite.attack(function(){
        entity.faceTile(me.sprite.isoX,me.sprite.isoY);
        entity.isHurted();
    });
};

EntityObject.prototype.getRange = function(sprite){ // returns number of case between 2 this.sprite & args[sprite]
    var row = Math.floor(this.sprite.isoY / this.game.tileSize);
    var col = Math.floor(this.sprite.isoX / this.game.tileSize);
    var sprite_row = Math.floor(sprite.isoY / this.game.tileSize);
    var sprite_col = Math.floor(sprite.isoX / this.game.tileSize);
    return Math.abs(row - sprite_row) + Math.abs(col - sprite_col);
};

EntityObject.prototype.destroy = function(){
    this.sprite.destroy();
    this.game.mapTiles.tilesGrid[this.tile.isoY / this.game.tileSize][this.tile.isoX / this.game.tileSize] =  0; // free tile
};

module.exports = EntityObject;