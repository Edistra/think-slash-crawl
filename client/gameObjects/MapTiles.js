'use strict';

var EntityObject = require('client/gameObjects/EntityObject');

var MapTiles = function(game, tilesGrid, tileSet) {
    this.game = game;
    this.tilesGrid = tilesGrid;
    this.tileSet = tileSet;
    this.game.tilesCache = []; // saves tile grid position & avoids tilesLayer loop to find a specific tile
    this.doors = [];
    this.block = null;
    this.mask = null;
    //this.createMask();
    this.create();
};

MapTiles.prototype.constructor = MapTiles;

MapTiles.prototype.create = function() {
    var tile, ennemy, block;
    for (var y = 0; y < this.tilesGrid.length; y++) {
        this.game.tilesCache[y] = [];
        for (var x = 0; x < this.tilesGrid[y].length; x++) {
            var n = this.tilesGrid[y][x];
            var isoX = x * this.game.tileSize;
            var isoY = y * this.game.tileSize;
           // var frames = ['stonebricks','stone','stonebrickscracked','cobblestone'];
            var frames = this.tileSet;
            var frame = frames[this.game.rnd.integerInRange(0,frames.length - 1)];
            //frame = ((x+y)%4 == 0) ? 'dirt' : 'grass';
            //tile = this.game.add.isoSprite(isoX, isoY, 0,'minecraft_tiles', 'planksoak', this.game.tilesLayer);
            tile = this.game.add.isoSprite(isoX, isoY, 0,'minecraft_tiles', frame, this.game.tilesLayer);
            tile.anchor.set(0.5, 0);
            tile.isTile = false;

            this.game.tilesCache[y][x] = tile;

            /*tile.inputEnabled = true;
            tile.events.onInputOver.add(this.onTileOver, tile);
            tile.events.onInputOut.add(this.onTileOut, tile);*/

            if([60,61,62,63,8].indexOf(n) > -1) tile.visible = false;

            if(n == 0){
                tile.isTile = true;
            }
            if(n == 1){
                var frames = ['stonebricks','stone','stonebrickscracked','stonebricksmossy'];
                var frame = frames[this.game.rnd.integerInRange(0,3)];
                frame = 'bricks';
               // block = this.game.add.isoSprite(isoX, isoY, 3,'minecraft_tiles', 'goldblock', this.game.tilesLayer);
                block = this.game.add.isoSprite(isoX, isoY, 3,'minecraft_tiles', frame, this.game.tilesLayer);
                block.anchor.setTo(0.5);
                this.block = block;
            }
            if(n == 6){
                frame = 'chest_christmas';
               // block = this.game.add.isoSprite(isoX, isoY, 3,'minecraft_tiles', 'goldblock', this.game.tilesLayer);
                block = this.game.add.isoSprite(isoX, isoY, 3,frame, 0, this.game.tilesLayer);
                block.anchor.setTo(0.5);
                block.scale.x = -1;
                this.block = block;
            }
            if(n == 9){
                frame = 'log_oak';
                block = this.game.add.isoSprite(isoX, isoY, 3,'minecraft_tiles', frame, this.game.tilesLayer);
                block.anchor.setTo(0.5);
                block = this.game.add.isoSprite(isoX, isoY, 45,'minecraft_tiles', frame, this.game.tilesLayer);
                block.anchor.setTo(0.5);
                block = this.game.add.isoSprite(isoX, isoY, 85,'minecraft_tiles', frame, this.game.tilesLayer);
                block.anchor.setTo(0.5);
                block = this.game.add.isoSprite(isoX, isoY, 127,'minecraft_tiles', frame, this.game.tilesLayer);
                block.anchor.setTo(0.5);
                block = this.game.add.isoSprite(isoX, isoY, 167,'minecraft_tiles', 'leavesoak', this.game.tilesLayer);
                block.anchor.setTo(0.5);
                block = this.game.add.isoSprite(isoX+this.game.tileSize, isoY, 167,'minecraft_tiles', 'leavesoak', this.game.tilesLayer);
                block.anchor.setTo(0.5);
                block = this.game.add.isoSprite(isoX-this.game.tileSize, isoY, 167,'minecraft_tiles', 'leavesoak', this.game.tilesLayer);
                block.anchor.setTo(0.5);
                block = this.game.add.isoSprite(isoX-this.game.tileSize, isoY-this.game.tileSize, 167,'minecraft_tiles', 'leavesoak', this.game.tilesLayer);
                block.anchor.setTo(0.5);
                block = this.game.add.isoSprite(isoX-this.game.tileSize, isoY+this.game.tileSize, 167,'minecraft_tiles', 'leavesoak', this.game.tilesLayer);
                block.anchor.setTo(0.5);
                block = this.game.add.isoSprite(isoX+this.game.tileSize, isoY+this.game.tileSize, 167,'minecraft_tiles', 'leavesoak', this.game.tilesLayer);
                block.anchor.setTo(0.5);
                block = this.game.add.isoSprite(isoX+this.game.tileSize, isoY-this.game.tileSize, 167,'minecraft_tiles', 'leavesoak', this.game.tilesLayer);
                block.anchor.setTo(0.5);
                block = this.game.add.isoSprite(isoX, isoY+this.game.tileSize, 167,'minecraft_tiles', 'leavesoak', this.game.tilesLayer);
                block.anchor.setTo(0.5);
                block = this.game.add.isoSprite(isoX, isoY-this.game.tileSize, 167,'minecraft_tiles', 'leavesoak', this.game.tilesLayer);
                block.anchor.setTo(0.5);
                /**/
                block = this.game.add.isoSprite(isoX+this.game.tileSize, isoY, 127,'minecraft_tiles', 'leavesoak', this.game.tilesLayer);
                block.anchor.setTo(0.5);
                block = this.game.add.isoSprite(isoX-this.game.tileSize, isoY, 127,'minecraft_tiles', 'leavesoak', this.game.tilesLayer);
                block.anchor.setTo(0.5);
                block = this.game.add.isoSprite(isoX-this.game.tileSize, isoY-this.game.tileSize, 127,'minecraft_tiles', 'leavesoak', this.game.tilesLayer);
                block.anchor.setTo(0.5);
                block = this.game.add.isoSprite(isoX-this.game.tileSize, isoY+this.game.tileSize, 127,'minecraft_tiles', 'leavesoak', this.game.tilesLayer);
                block.anchor.setTo(0.5);
                block = this.game.add.isoSprite(isoX+this.game.tileSize, isoY+this.game.tileSize, 127,'minecraft_tiles', 'leavesoak', this.game.tilesLayer);
                block.anchor.setTo(0.5);
                block = this.game.add.isoSprite(isoX+this.game.tileSize, isoY-this.game.tileSize, 127,'minecraft_tiles', 'leavesoak', this.game.tilesLayer);
                block.anchor.setTo(0.5);
                block = this.game.add.isoSprite(isoX, isoY+this.game.tileSize, 127,'minecraft_tiles', 'leavesoak', this.game.tilesLayer);
                block.anchor.setTo(0.5);
                block = this.game.add.isoSprite(isoX, isoY-this.game.tileSize, 127,'minecraft_tiles', 'leavesoak', this.game.tilesLayer);
                block.anchor.setTo(0.5);
            }
            if(n == 3){
                var frames = ['stonebricks','stone','stonebrickscracked','stonebricksmossy'];
                var frame = frames[this.game.rnd.integerInRange(0,3)];
                block = this.game.add.isoSprite(isoX, isoY, 3,'minecraft_tiles', frame, this.game.tilesLayer);
                block.anchor.setTo(0.5);
                var frame = frames[this.game.rnd.integerInRange(0,3)];
                block = this.game.add.isoSprite(isoX, isoY, 45,'minecraft_tiles', frame, this.game.tilesLayer);
                block.anchor.setTo(0.5);
                block = this.game.add.isoSprite(isoX, isoY, 85,'minecraft_tiles', frame, this.game.tilesLayer);
                block.anchor.setTo(0.5);
            }
            if(n == 40 || n == 41){
                console.log('add ennemy');
                if(n == 40)
                    var ennemy = new EntityObject(this.game, isoX, isoY, 0, 'orc');
                else if(n == 41)
                    var ennemy = new EntityObject(this.game, isoX, isoY, 0, 'jakk_xmas');
               // var ennemy = new EntityObject(this.game, isoX, isoY, 0, 'moonlight_flower');
                ennemy.sprite.anchor.setTo(0.5,0.7);
                ennemy.sprite.standAnim = false;
                ennemy.setAsEnnemy();
                ennemy.ref = n;
                this.game.tilesLayer.add(ennemy.sprite);

                ennemy.sprite.events.onInputDown.add(ennemy.isAttacked, ennemy);
                ennemy.sprite.events.onInputOver.add(function() { this.game.cursor.play('attack'); }, this);
                ennemy.sprite.events.onInputOut.add(function() { this.game.cursor.play('pointer'); }, this);
            }
            if(n == 5){
                console.log('add player');
                this.game.player = new EntityObject(this.game, isoX, isoY, 0, 'knight');
                this.game.player.ref = n;
                this.game.player.sprite.anchor.setTo(0.5,0.7);
                this.game.tilesLayer.add(this.game.player.sprite);
                this.game.player.tile = tile;
                this.game.player.sprite.isPlayer = true;
                this.game.player.max_ap = 6;
                this.game.player.hp = 3;

            }
            if([60,61,62,63].indexOf(n) > -1){ // add door
                var door = this.game.add.isoSprite(isoX, isoY, 0,'door', 0, this.game.tilesLayer);
                door.anchor.setTo(0.1,0.7);
                if([61,63].indexOf(n) > -1)  door.scale.x = -1;
                if([62,63].indexOf(n) > -1)  door.anchor.setTo(0.9,0.8);
                door.tile = tile;
                door.inputEnabled = true;
                door.events.onInputOver.add(this.onDoorOver, this);
                door.events.onInputOut.add(function() { this.game.cursor.play('pointer'); }, this);
                door.events.onInputDown.add(this.onDoorClick, this);
                this.doors[this.doors.length] = door;

                door.index_row = y;
                door.index_col = x;
                door.dest_row = 'same';
                door.dest_col = 'same';
                door.floorX = null;
                door.floorY = null;
                if(n == 60){
                    door.floorX = this.game.floor.currentRoom.floorX - 1;
                    door.floorY = this.game.floor.currentRoom.floorY;
                    door.dest_col = 'right';
                }
                else if(n == 61){
                    door.floorX = this.game.floor.currentRoom.floorX;
                    door.floorY = this.game.floor.currentRoom.floorY - 1;
                    door.dest_row = 'bottom';
                }
                else if(n == 62){
                    door.floorX = this.game.floor.currentRoom.floorX + 1;
                    door.floorY = this.game.floor.currentRoom.floorY;
                    door.dest_col = 'left';
                    door.alpha = 0.6;
                }
                else if(n == 63){
                    door.floorX = this.game.floor.currentRoom.floorX;
                    door.floorY = this.game.floor.currentRoom.floorY + 1;
                    door.dest_row = 'top';
                    door.alpha = 0.6;
                }

                if(n == 60 || n == 61){ // add block above a door
                    var frames = ['stonebricks','stone','stonebrickscracked','stonebricksmossy'];
                    var frame = frames[this.game.rnd.integerInRange(0,3)];
                    block = this.game.add.isoSprite(isoX, isoY, 85,'minecraft_tiles', frame, this.game.tilesLayer);
                    block.anchor.setTo(0.5);
                }
            }
        }
    }
};

MapTiles.prototype.onDoorClick = function(door) {
    if(this.game.player.getRange(door) == 1){
        console.log('open');
        this.game.floor.changeRoom(door);
        this.game.cursor.play('pointer');
    }
};

MapTiles.prototype.onDoorOver = function(door) {
    if(this.game.player.getRange(door) == 1)
        this.game.cursor.play('door');
};

MapTiles.prototype.onGridClick = function() {
    var me = this;
    if(!this.game.player.moving){ // && this.game.turnManager.currentEntity == this.game.player){
        this.game.tilesLayer.forEach(function (tile) {
            if(tile.isoBounds.containsXY(me.game.cursorPos.x, me.game.cursorPos.y)){
                var dst_index_row = Math.floor(tile.isoY/me.game.tileSize);
                var dst_index_col = Math.floor(tile.isoX/me.game.tileSize);
                console.log(dst_index_row,dst_index_col);
                if(me.game.mapTiles.isFree(tile))
                    me.game.player.moveInPath(dst_index_col, dst_index_row);
            }
        });
    }
};

MapTiles.prototype.update = function() {
    var me = this;
    if(!this.game.player.moving){
        var count = 0;
        this.game.tilesLayer.forEach(function (tile) { // hover effect on tiles
            if(tile.isTile){
                var inBounds = tile.isoBounds.containsXY(me.game.cursorPos.x, me.game.cursorPos.y);
                if(inBounds)    count++;
                if (!tile.selected && inBounds) {
                    tile.selected = true;

                    if(me.game.player.getRange(tile) <= me.game.player.mp){
                        console.log('can move');
                        tile.tint = 0x78dd77;
                        me.game.ui.showPath(tile,me.game.player.mp);
                        // color all the way to tile
                    }else{
                        me.game.ui.cleanPath();                        
                    }
                }else if (tile.selected && !inBounds) {
                    tile.selected = false;
                    tile.tint = 0xffffff;
                }
            }
        });
        if(count == 0)  me.game.ui.cleanPath();
    }
    /* alpha mask */
    /*if(this.game.player != undefined && this.block != undefined){
        var p = this.game.player.sprite;
        var ts = this.game.tileSize;
        if((this.tilesGrid[Math.floor(p.isoY/ts)+1][Math.floor(p.isoX/ts)] == 1) ||
           (this.tilesGrid[Math.floor(p.isoY/ts)+1][Math.floor(p.isoX/ts)+1] == 1) ||
           (this.tilesGrid[Math.floor(p.isoY/ts)][Math.floor(p.isoX/ts)+1] == 1)){
            this.mask.x = this.game.player.sprite.x - 50;
            this.mask.y = this.game.player.sprite.y - 65;
            this.block.mask = this.mask;
            this.mask.visible = true;
        }else{
            this.block.mask = null;
            this.mask.visible = false;
        }
    }*/
    this.game.iso.topologicalSort(this.game.tilesLayer,10);
};

MapTiles.prototype.isFree = function(tile){
    return (this.tilesGrid[Math.floor(tile.isoY/this.game.tileSize)][Math.floor(tile.isoX/this.game.tileSize)] == 0);
};


MapTiles.prototype.createMask = function(tile){
    var w = 100;
    var h = 100;
    this.mask = this.game.add.graphics(0,0);
    this.mask.beginFill(0xFFFFFF);
    /*this.mask.moveTo(0,0);
    this.mask.lineTo(w,0);
    this.mask.lineTo(w,h);
    this.mask.lineTo(w/2 + w/10,h);
    this.mask.bezierCurveTo(w/1.2,h/1.5,w/1.2,h/3,w/2 + w/10,0);
    this.mask.lineTo(w/2 - w/10,0);
    this.mask.bezierCurveTo(w/5,h/3,w/5,h/1.5,w/2 - w/10,h);
    this.mask.lineTo(0,h);
    this.mask.lineTo(0,0);*/

    this.mask.moveTo(-w,-h);
    this.mask.lineTo(w*2,-h);
    this.mask.lineTo(w*2,h*2);
    this.mask.lineTo(w/2 + w/10,h*2);
    this.mask.lineTo(w/2 + w/10,h);
    this.mask.bezierCurveTo(w/1.2,h/1.5,w/1.2,h/3,w/2 + w/10,0);
    this.mask.bezierCurveTo(w/2 + 3,h-h*1.10,w/2 - 3,h-h*1.10,w/2 - w/10,0);
    this.mask.bezierCurveTo(w/5,h/3,w/5,h/1.5,w/2 - w/10,h);
    this.mask.bezierCurveTo(w/2 - 3,h*1.10,w/2 + 3,h*1.10,w/2 + w/10,h);
    this.mask.lineTo(w/2 + w/10,h);
    this.mask.lineTo(w/2 + w/10,h*2);
    this.mask.lineTo(-w,h*2);
    this.mask.lineTo(-w,-h);
};

MapTiles.prototype.destroy = function(){
    this.game.tilesLayer.destroy(true);
   /* this.game.tilesLayer.children.forEach(function(child){
        if(child.isPlayer)  console.log('kill');//child.kill();
        else child.destroy();
    });*/
    this.game.tilesLayer = this.game.add.group();
    this.game.cursor.play('pointer');
    this.game.world.bringToTop(this.game.cursor);
};

module.exports = MapTiles;