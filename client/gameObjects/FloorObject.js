'use strict';

var MapTiles     = require('client/gameObjects/MapTiles');

var FloorObject = function(game) {
    this.game = game;
    this.data = this.game.cache.getJSON('floor')['floor_2'];
    this.currentRoom = this.game.cache.getJSON('grid')[this.data.start];
    this.matrix = this.data.matrix;
    for(var y = 0; y < this.matrix.length; y++){
        for(var x = 0; x < this.matrix[y].length; x++){
            if(this.matrix[y][x] == this.data.start){
                this.currentRoom.floorX = x;
                this.currentRoom.floorY = y;
            }
        }
    }
    /* Pathfinder settings */
    this.game.pathfinder = new EasyStar.js();
    this.game.pathfinder.setAcceptableTiles([0]);
    this.game.pathfinder.disableDiagonals();
    this.game.pathfinder.disableSync();

};

FloorObject.prototype.constructor = FloorObject;

FloorObject.prototype.removePlayer = function() { // remove player ref from map grid to avoid duplication
    for(var y = 0; y < this.currentRoom.matrix.length; y++){
        for(var x = 0; x < this.currentRoom.matrix[y].length; x++){
            if(this.currentRoom.matrix[y][x] == 5){
                this.currentRoom.matrix[y][x] = 0;
            }
        }
    }
};

FloorObject.prototype.changeRoom = function(door) {
    this.game.mapTiles.destroy();
    console.log(door.floorY, door.floorX);
    this.currentRoom = this.game.cache.getJSON('grid')[this.matrix[door.floorY][door.floorX]];
    this.currentRoom.floorX = door.floorX;
    this.currentRoom.floorY = door.floorY;
    var player_row = door.index_row;
    var player_col = door.index_col;
    if(door.dest_row == 'bottom')   player_row = this.currentRoom.nb_row - 2;
    if(door.dest_row == 'top')      player_row = 1;
    if(door.dest_col == 'left')     player_col = 1;
    if(door.dest_col == 'right')    player_col = this.currentRoom.nb_col - 2;
    this.removePlayer();
    this.currentRoom.matrix[player_row][player_col] = 5;
    this.game.turnManager.resetEntities();
    this.initMapTiles();
};

FloorObject.prototype.initMapTiles = function() {
    //this.game.world.setBounds(0, 0, (this.currentRoom.matrix.length*2)*60, (this.currentRoom.nb_col)*50);
    this.game.mapTiles = new MapTiles(this.game, this.currentRoom.matrix, this.currentRoom.tileset); // map generation
    this.game.pathfinder.setGrid(this.game.mapTiles.tilesGrid);
    this.game.turnManager.sortEntities();
    this.game.turnManager.resetTurns();
    this.game.ui.showEntitiesTurns();
};

module.exports = FloorObject;