"use strict";

var gameBootstrapper = {
  init: function init(containerId) {
  	var game;
    //game = new Phaser.Game(640, 640, Phaser.AUTO, containerId);
    game = new Phaser.Game(window.innerWidth * window.devicePixelRatio, window.innerHeight * window.devicePixelRatio, Phaser.AUTO, containerId);

    game.state.add('menu',require('./states/menu'));
    //game.state.add('over',require('./states/over'));
    game.state.add('play',require('./states/play'));
    game.state.start('play');
    console.log('Game initialized.');
  }
};

module.exports = gameBootstrapper;