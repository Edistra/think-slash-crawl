'use strict';

var UI = function(game) {
    this.game = game;
    this.ui_layer = this.game.add.group();
    this.grayfilter = this.game.add.filter('Gray');
};

UI.prototype.constructor = UI;

UI.prototype.cleanPath = function() { // clean path showing 
    var c = this.game.tilesCache;
    for (var y = 0; y < c.length; y++)
        for (var x = 0; x < c[y].length; x++)
            c[y][x].tint = 0xffffff;
};

UI.prototype.showPath = function(tile,mp) { // show the path to a certain tile
    this.cleanPath();
    var me = this;
    var pathfinder = this.game.pathfinder;
    var source_row = Math.floor(this.game.player.sprite.isoY/this.game.tileSize);
    var source_col = Math.floor(this.game.player.sprite.isoX/this.game.tileSize);
    var target_row = Math.floor(tile.isoY/this.game.tileSize);
    var target_col = Math.floor(tile.isoX/this.game.tileSize);
    pathfinder.findPath(source_col, source_row, target_col, target_row, function( path ) {
        if(path === null) {
            console.log("The path to the destination point was not found.");
        } else {
            for(var i = 0; i < path.length; i++) {
                if(i > 0) { // dont tint player current tile 
                    var t = me.game.tilesCache[path[i].y][path[i].x];
                    if(i <= mp) // reachable by player
                        t.tint = 0x78dd77;
                    else
                        t.tint = 0xfc000f;
                }
            }
        }
    });
    pathfinder.calculate();
};

UI.prototype.showEntitiesTurns = function() { // create right UI showing current entities turn
    var me = this;
    var style = { font: "16px Courier", fill: "#333", tabs: [ 164, 120, 80 ] };
    this.ui_layer.destroy(true);
    this.ui_layer = this.game.add.group();
    this.ui_layer.fixedToCamera = true;
    this.game.turnManager.entities.some(function(entity, index) {
        var icon = me.game.add.sprite(0, me.ui_layer.y - index * 130,'icon_'+entity.ref);
        if(entity == me.game.turnManager.currentEntity){
            console.log(entity.ref+' is playing');
            var shape = me.game.add.sprite(icon.x - 2,icon.y - 2,'icon_shape');
            shape.width = icon.width + 4;
            shape.height = icon.height + 70;
            shape.tint = '0xffffba';
            me.ui_layer.add(shape);
            var style = { font: "16px Courier", fill: "#333", tabs: [ 164, 120, 80 ] };
        }else{
            icon.filters = [me.grayfilter];
            var style = { font: "16px Courier", fill: "#fff", tabs: [ 164, 120, 80 ] };
        }
        me.ui_layer.add(icon);
        entity.text_hp = me.game.add.text(0, me.ui_layer.y - index * 130 + icon.height + 10, entity.hp + 'HP', style);
        entity.text_mp = me.game.add.text(0, me.ui_layer.y - index * 130 + icon.height + 40, entity.mp + 'MP', style);
        entity.text_ap = me.game.add.text(me.ui_layer.width / 2, me.ui_layer.y - index * 130 + icon.height + 40, entity.ap + 'AP', style);
        me.ui_layer.add(entity.text_hp);
        me.ui_layer.add(entity.text_mp);
        me.ui_layer.add(entity.text_ap);
    });

    this.ui_layer.cameraOffset.x = 100;
    this.ui_layer.cameraOffset.y = 450;
};

module.exports = UI;