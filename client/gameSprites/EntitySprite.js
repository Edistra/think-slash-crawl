'use strict';

var EntitySprite = function(game, x, y, z, sprite) {
    Phaser.Plugin.Isometric.IsoSprite.call(this, game, x, y, z, sprite);
    /*var shadow = game.add.sprite(0,0,'shadow');
    shadow.anchor.setTo(0.5,0);
    shadow.tint = 0x000000;
    shadow.alpha = 0.6;
    this.addChild(shadow);*/
    this.dead = false;
    this.standAnim = true;
    this.game = game;
    this.key = sprite;
    this.setupAnimations();
    this.ext = '';
    this.animationCompleteCallback = null;
};

EntitySprite.prototype = Object.create(Phaser.Plugin.Isometric.IsoSprite.prototype);
EntitySprite.prototype.constructor = EntitySprite;

EntitySprite.prototype.animationComplete = function() {
    if(this.animationCompleteCallback){
        this.animationCompleteCallback();
        this.animationCompleteCallback = null; 
    }

    if(this.dead){
        this.animations.stop();
        return;
    }

    var anim = this.animations.currentAnim.name;

    if(anim.indexOf('attack') >= 0 || anim.indexOf('hurt') >= 0)
        this.stand();

    /*if(this.ext != '')
        this.anchor.setTo(1.2, 0.3);
    else
        this.anchor.setTo(0.5, 0.3);*/
};

EntitySprite.prototype.setupAnimations = function() {
    //this.anchor.setTo(0.5, 0.5);
    this.animations.add('stand',["stand_00","stand_01","stand_02","stand_03","stand_04","stand_05","stand_06"],5,true);
    this.animations.add('stand_up',["stand_up_00","stand_up_01","stand_up_02","stand_up_03","stand_up_04","stand_up_05","stand_up_06"],5,true);
    this.animations.add('walk',["walk_00","walk_01","walk_02","walk_03","walk_04","walk_05","walk_06","walk_07","walk_08"],10,true);
    this.animations.add('walk_up',["walk_up_00","walk_up_01","walk_up_02","walk_up_03","walk_up_04","walk_up_05","walk_up_06","walk_up_07","walk_up_08"],10,true);
    this.animations.add('attack',["attack_00","attack_01","attack_02","attack_03","attack_04"],10,false);
    this.animations.add('attack_up',["attack_up_00","attack_up_01","attack_up_02","attack_up_03","attack_up_04"],10,false);
    this.animations.add('hurt',["hurt","hurt_00","hurt_01"],10,false);
    this.animations.add('hurt_up',["hurt_up","hurt_up_00","hurt_up_01"],10,false);
    this.animations.add('dying',["dying"],10,false);

    if(this.game.cache.getFrameByName(this.key,'dying_up') != null){
        this.animations.add('dying_up',["dying_up"],10,false);
    }else{
        this.animations.add('dying_up',["dying"],10,false);  
    }
    this.animations.add('dead',["dead"],10,false);
    this.animations.add('dead_up',["dead"],10,false);
    this.events.onAnimationComplete.add(this.animationComplete, this);
};

EntitySprite.prototype.stand = function(){
    console.log('stand');
    this.animations.play("stand"+this.ext);
    if(!this.standAnim) this.stopAnimation();
};
EntitySprite.prototype.walk = function(){
    this.animations.play("walk"+this.ext);
};
EntitySprite.prototype.attack = function(callback){
    this.animationCompleteCallback = callback;
    this.animations.play("attack"+this.ext);
};
EntitySprite.prototype.hurt = function(){
    this.animations.play("hurt"+this.ext);
};
EntitySprite.prototype.dying = function(callback){
    //console.log(this.game.cache.getFrameByName('dying'));
    this.dead = true;
    //this.animationCompleteCallback = callback;
    this.animations.play("hurt"+this.ext);  
    //this.anchor.setTo(0.5,0);
    this.tint = 0xfc000f;
    var t = this.game.add.tween(this).to({alpha: 0}, 1000, Phaser.Easing.Linear.None, true);
    t.onComplete.add(callback);

};
EntitySprite.prototype.faceRight = function(){
    this.scale.x = -1;
};
EntitySprite.prototype.faceLeft = function(){
    this.scale.x = 1;
};
EntitySprite.prototype.stopAnimation = function(){
    this.animations.stop();
};

module.exports = EntitySprite;