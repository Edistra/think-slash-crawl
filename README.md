# Think Slash Crawl

[Démo](http://www.edistra.com/think_slash_crawl/public/) le déplacement n'est volontairement pas limité au nombre d'AP du personnage / la barre d'espace entraîne la "fin de tour" et laisse la main aux personnages contrôlés par l'IA

### Objectif
Un Dungeon crawler / tactical RPG

### Dépendances
* Le framework [Phaser](http://phaser.io/) (2.4.3) pour ses outils relatifs à la manipulation de Canvas
* Le plugin [Phaser-Isometric](https://github.com/lewster32/phaser-plugin-isometric) pour la gestion de la projection axonométrique.
* La librairie [EasyStarJS](http://www.easystarjs.com/) pour tout ce qui est pathfinding

### Organisation du code
En respectant les conventions par défaut de brunch on va retrouver index.html dans client/assets qui se charge d'appeler libraries.js, contenant les dépendances gérées avec bower, et client.js contenant le code principal de l'application instancié par client/game.js qui va appeler le state states/play.js

##### play.js
On retrouve ici tout ce qui est loading des assets dans preload(), les instanciations dans create() et gestion de la game loop dans update()

##### gameSprites/UI.js
qui est consacré à l'interface utilisateur
##### gameObjects/TurnManager.js
pour la gestion du tour par tour
##### gameObjects/FloorObject.js
qui va surtout supperviser les MapTiles, leur init et ce qui en dépend, c'est à dire UI, TurnManager mais aussi pathfinder
##### gameObjects/MapTiles.js
pour la génération de la map (création des tiles, des entities) et les events qui vont avec
##### gameObjects/EntityObject.js
pour tout ce qui concerne un perso avec lequel le joueur peut interragir, gère les actions comme les déplacements, les attaques, etc.
##### gameSprites/EntitySprite.js
relié directement à un EntityObject pour tout ce qui est gestion d'animation et qui étend IsoSprite

##### gameAssets/
Contient toutes les ressources graphiques du jeu et les maps au format json
