module.exports = config:
  paths:
    "watched": ["client", "vendor"]
    "public": "public"
  files:
    javascripts:
      joinTo:
        'js/libraries.js': /^(vendor|bower_components)/
        'js/client.js': /^client/
 
      order:
        before: [
          'bower_components/phaser/build/phaser.js'
          'bower_components/easystarjs/bin/easystar-0.2.3.min.js'
          'bower_components/phaser-plugin-isometric/dist/phaser-plugin-isometric.min.js'
        ]
 
    stylesheets: joinTo: 'styles/client.css'
 
  server:
    run: yes
    port: 3333
 
  plugins:
    autoReload:
      port: 3334